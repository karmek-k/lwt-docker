FROM php:8.1-fpm-alpine

# TODO: separate the version string from this URL
RUN curl -sLo lwt_wrap.zip \
    https://sourceforge.net/projects/learning-with-texts/files/learning_with_texts_2_0_3_complete.zip/download
RUN unzip -d wrap lwt_wrap.zip \
    && unzip -d . wrap/lwt*.zip \
    && rm -r wrap lwt_wrap.zip

RUN apk add oniguruma-dev nginx
RUN docker-php-ext-install mbstring mysqli
RUN rm /etc/nginx/http.d/* \
    && ln -s /dev/stdout /var/log/nginx/access.log \
    && ln -s /dev/stderr /var/log/nginx/error.log

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
    && echo -e "<?php\n\
\$server = getenv('DB_HOST');\n\
\$userid = getenv('DB_USER');\n\
\$passwd = getenv('DB_PASSWORD');\n\
\$dbname = getenv('DB_NAME');\n" > connect.inc.php \
    && sed -i 's/;\(date.timezone =\)/\1 ;;TIMEZONE;;/' /usr/local/etc/php/php.ini

ADD ./vhost.conf /etc/nginx/http.d/lwt.conf

ADD ./entrypoint.sh /etc/entrypoint.sh

EXPOSE 8000
CMD ["sh", "/etc/entrypoint.sh"]