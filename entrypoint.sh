#!/usr/bin/env sh

# set timezone
sed -i "s|;;TIMEZONE;;|${TIMEZONE:-Europe/London}|" /usr/local/etc/php/php.ini

php-fpm &
nginx -g 'daemon off;' &

# graceful shutdown
trap "jobs -p | xargs kill -s SIGTERM; exit 0" SIGINT SIGQUIT SIGTERM

# run until nginx/fpm terminates
wait -n
exit $?
